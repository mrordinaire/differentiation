module Differentiation where

data Expr a = Lit a
            | Var String
            | Add (Expr a) (Expr a)
            | Mul (Expr a) (Expr a)
            | Negate (Expr a)
  deriving (Eq, Show)

instance Num a => Num (Expr a) where
  (+)    = Add
  (*)    = Mul
  negate = Negate

differentiate :: Num a => Expr a -> Expr a
differentiate (Lit _)     = Lit 0
differentiate (Var "x")   = Lit 1
differentiate (Var _)     = Lit 0
differentiate (Negate e)  = Negate (differentiate e)
differentiate (Add e1 e2) = Add (differentiate e1) (differentiate e2)
differentiate (Mul e1 e2) = Mul (differentiate e1) (differentiate e2)
